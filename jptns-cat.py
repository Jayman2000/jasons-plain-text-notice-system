"""Display a file and its Notice References.

jptns-cat is a utility that displays the contents of a file. Instead of showing
JPTNS Notice References, this utility shows the appropriate notice for that
reference.

🅭🄍1.0

This was dedicated to the public domain using the CC0 1.0 Universal Public
Domain Dedication <https://creativecommons.org/publicdomain/zero/1.0/>.

Author: 2021 Jason Yundt <swagfortress@gmail.com>
"""
from pathlib import Path
from sys import stderr

import jptns


# This prevents tools from counting this variable as a Notice Reference.
NOTICE_REFERENCE = "JPTNS-Notice" + ":"

args = jptns.args(description=__doc__)
project_root_folder = jptns.project_root_folder()


def process_line(line):
	start = line.find(NOTICE_REFERENCE)
	if start == -1:
		print(line, end="")
	else:
		print(line[:start], end="")
		relative_path = line[start + 13:-1]
		notice = jptns.find_notice(project_root_folder, path, Path(relative_path))
		if notice is None:
			print(f"ERROR: Couldn’t find Notice File for “{line[start:]}”.", file=stderr)
			print(line[start:], end="")
		else:
			print(notice, end="")


for path in args.paths:
	try:
		with path.open('r') as file:
			for line in file:
				process_line(line)
	except FileNotFoundError:
		print(f"ERROR: Couldn’t open “{path}”. File not found.")
	except IsADirectoryError:
		print(f"ERROR: “{path}” is a directory.")
