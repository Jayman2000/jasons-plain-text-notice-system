from argparse import ArgumentParser
from pathlib import Path


NOTICES = Path("NOTICES")


def args(description=None):
	parser = ArgumentParser(description=description)
	parser.add_argument('paths', metavar='FILE', nargs='+', type=Path)
	return parser.parse_args()


def project_root_folder():
	project_root_folder = Path.cwd()
	print(f"Using “{project_root_folder}” as the Project Root Folder…")
	return project_root_folder


# 5. FINDING A NOTICE REFERENCE’S NOTICE FILE
# When encountering a File in the Project that contains a Notice Reference,
# here’s how to determine which Notice File it’s referring to:
# 1. The Relative Path is the Notice File’s Relative Path (the one in the Notice
# Reference).
def find_notice(project_root_folder, file_in_the_project_path, relative_path):
	# 2. The Current Folder is the folder that that File in the Project is
	# in.
	current_folder = file_in_the_project_path.parent
	while True:
		notices_folder = Path(current_folder, NOTICES)
		# 3. If there’s a NOTICES Folder in the Current Folder, then
		if notices_folder.is_dir():
			# A. That NOTICES Folder is the Current NOTICES Folder.
			# B. Create a Candidate Path by combining the Current
			# NOTICES Folder’s path with the Relative Path.
			candidate_path = Path(notices_folder, relative_path)
			# C. If a file exists at the Candidate Path, then stop.
			# That file is the one we’re looking for.
			try:
				with candidate_path.open('r') as file:
					return file.read()
			except FileNotFoundError:
				pass
			except IsADirectoryError:
				pass
		# 4. The Current Folder is now the folder that the old Current
		# Folder is in.
		current_folder = current_folder.parent
		# 5. If the Current Folder is the Project Root Folder, then
		# stop. No matching Notice File was found.
		if current_folder == project_root_folder:
			return None
		# 6. Go to step 3.
